﻿using Microsoft.Xna.Framework;
using System;
using System.Globalization;
using System.IO;
using Terraria;
using Terraria.Localization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using Player = Terraria.Player;

namespace uMod.Game.Terraria
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class TerrariaPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly Player player;
        private readonly int whoAmI;

        public TerrariaPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
            int.TryParse(playerId, out whoAmI);
        }

        public TerrariaPlayer(int playerId, string playerName)
        {
            // Store player details
            Id = playerId.ToString();
        }

        public TerrariaPlayer(Player player) : this(player.whoAmI, player.name)
        {
            // Store player object(s)
            this.player = player;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object => player;

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address
        {
            get
            {
                RemoteClient remoteClient = Netplay.Clients[whoAmI];
                return remoteClient != null ? Netplay.Clients[whoAmI].Socket.GetRemoteAddress().GetIdentifier() : string.Empty;
            }
        }

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Returns if the player is a server admin
        /// </summary>
        public override bool IsAdmin => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Returns if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned
        {
            get
            {
                RemoteClient remoteClient = Netplay.Clients[whoAmI];
                return remoteClient != null ? Netplay.IsBanned(remoteClient.Socket.GetRemoteAddress()) : false;
            }
        }

        /// <summary>
        /// Returns if the player is connected
        /// </summary>
        public bool IsConnected
        {
            get
            {
                RemoteClient remoteClient = Netplay.Clients[whoAmI];
                return remoteClient != null ? remoteClient.Socket.IsConnected() : false;
            }
        }

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => !IsDead;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => player?.dead ?? false;

        /// <summary>
        /// Returns if the player is sleeping
        /// </summary>
        public bool IsSleeping => false;

        /// <summary>
        /// Returns if the player is the server
        /// </summary>
        public bool IsServer => false;

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban player
                Netplay.AddBan(whoAmI);

                // Kick player with reason
                Kick(reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => IsBanned ? TimeSpan.MaxValue : TimeSpan.Zero;

        /// <summary>
        /// Kicks the player from the game
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                NetMessage.SendData(2, whoAmI, -1, NetworkText.FromLiteral(reason));
                //NetMessage.BootPlayer(this.Id, NetworkText.FromKey("Net.CheatingProjectileSpam", new object[0]));
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                if (File.Exists(Netplay.BanFilePath))
                {
                    using (StreamWriter writer = new StreamWriter(Netplay.BanFilePath))
                    {
                        foreach (string line in File.ReadAllLines(Netplay.BanFilePath))
                        {
                            if (!line.Contains($"//{Name}") && !line.Contains(Address))
                            {
                                writer.WriteLine(line);
                            }
                        }
                    }
                }
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount)
        {
            if (player != null)
            {
                NetMessage.SendData(35, -1, -1, NetworkText.Empty, whoAmI, amount);
            }
        }

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
            set
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
            set
            {
                throw new NotImplementedException(); // TODO: Implement when possible
            }
        }

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount) => NetMessage.SendData(84, -1, -1, NetworkText.Empty, whoAmI, amount);

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => NetMessage.SendData(21, -1, -1, NetworkText.Empty, whoAmI);

        /// <summary>
        /// Renames the player to specified name
        /// </summary>
        /// <param name="newName"></param>
        public void Rename(string newName)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position()
        {
            if (player != null)
            {
                return new Position(player.position.X, player.position.Y, 0);
            }

            return new Position();
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && player != null)
            {
                player.Teleport(new Vector2(x, y), 1);
                NetMessage.SendData(65, -1, -1, NetworkText.Empty, 0, whoAmI, x, y, 1);
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToTerraria(message), args) : Formatter.ToUnity(message);
                NetMessage.SendData(25, whoAmI, -1, NetworkText.FromLiteral(prefix != null ? $"{prefix} {message}" : message));
                //NetMessage.SendChatMessageToClient(NetworkText.FromLiteral(Main.motd), new Color(255, 240, 20), plr);
                //NetMessage.SendChatMessageToClient(NetworkText.FromKey("Game.JoinGreeting", new object[] { str }), new Color(255, 240, 20), plr);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Chat and Commands
    }
}
